ESP8266
=======

# ESP-07

## Firmwares

### Stock

#### Programming
 - [Arduino](https://github.com/esp8266/Arduino)

### NodeMCU
[NodeMCU](https://github.com/nodemcu/nodemcu-firmware) is based on the microcontroller firmware [eLua](http://www.eluaproject.net/)(embedded lua) and various other open source projects. NodeMCU is licensed under [MIT](https://github.com/nodemcu/nodemcu-firmware/blob/master/LICENSE). \
You can build NodeMCU from source or use this web-based [custom firmware-builder](http://frightanic.com/nodemcu-custom-build/). (I suggest to use some disposable email address service: Guerrilla Mail, Mailinator, 10minutemail).

#### Programming
 - [API Documentation](https://github.com/nodemcu/nodemcu-firmware/wiki/nodemcu_api_en)

## Tools
### Serial console
Add user to 'modem' group:
 - $`usermod -a -G modem $USER`
Logout and Login again to apply that your user was added to the modem group.

To connect with the ESP use a USB-TTY Adapter with 3.3 Volts:

| TTY-Adapter | ESP8266|
|-------------|--------|
|   3.3 Volts | VCC    |
|         GND | GND    |
|          TX | RX     |
|          RX | TX     |

Afterwords connect the adapter via. USB with your computer and then type:
- $`minicom -b 9600 -D /dev/ttyUSB0`

You probably need to install `minicom`. To quit minicom push `CTRL+a` and then `q`. Good minicom introduction is available at [cyberciti.biz](http://www.cyberciti.biz/tips/connect-soekris-single-board-computer-using-minicom.html).

### Flashing

#### esptool.py
For flashing I'd suggest you to use [esptool.py](https://github.com/themadinventor/esptool).

##### Installation
Home directory:\
Create .local folder in your home directory\
$`mkdir ~/.local/`
To make files in `~/.local/bin/` 'executable' you need to add it to your \$PATH variable. To do so add `PATH="$HOME/.local/bin:$PATH"` to your `~/.bashrc`, or `~/.zshrc`. (If you have no clue what you are doing you can simply run $`echo 'PATH="$HOME/.local/bin:$PATH"' >> ~/.bashrc`).\
Alternatively you can run the command $`export PATH="$HOME/.local/bin:$PATH"` which resoluts in the same resolut, but only temproray.\

Afterwords:
 - $`git clone git@github.com:themadinventor/esptool.git`
 - $`cd ./esptool`
 - $`python2.7 setup.py install --user`

If you are missing dependencies see [esptool README](https://github.com/themadinventor/esptool/blob/master/README.md).


#### Boot in flash mode
Before the firmware initializes, the bootloader is looking for a "LOW" signal on GPIO Zero. To "produce" LOW you need to connect GPIO0 with GND. This tells the bootloader to run the flash upload mode. If you are in the upload mode you use `esptool.py` commands.

### Toolchain
Before you can build any firmware you need to get [the toolchain](https://github.com/pfalcon/esp-open-sdk) running.\
To build the toolchain install the mentioned dependencies as mentioned in their [README.md](https://github.com/pfalcon/esp-open-sdk/blob/master/README.md).\
Clone their repository & move into the cloned repository:
 - $`git clone --recursive https://github.com/pfalcon/esp-open-sdk.git`
 - $`cd esp-open-sdk`

Download additional files and build the SDK:
 - $`make STANDALONE=n --jobs=8`

Note: I'm using `--jobs` for speeding up the build process. See [this posting](http://blog.jgc.org/2015/03/gnu-make-insanity-finding-value-of-j.html) for more details.

To run the toolchain you need to add it's executeables to the PATH variable again:
- $`export PATH=$(pwd)/xtensa-lx106-elf/bin:$PATH`

### Building NodeMCU
... export xtensa $PATH as mentioned before ...
git clone nodemcu ... cd nodemcu
and build with $`make` (be careful, using --jobs will not work for nodemcu)
other commands are $`make flash`

## Hardware
### Pinout
At esp8266 wiki there is [a list](http://www.esp8266.com/wiki/doku.php?id=esp8266-module-family) of which PIN does what.

#Addtitional Links
- [Getting Started with ESP8266](http://telecnatron.com/articles/Getting-Started-With-The-ESP8266-WIFI-Microcontroller/index.html)
  - A useful collection of how to get nodemcu running on the ESP8266.
- [ESP8266 Git Project Page](https://github.com/esp8266)
- [Adafruit Tutorial NodeMCU ESP8266](https://learn.adafruit.com/adafruit-huzzah-esp8266-breakout/using-nodemcu-lua)
  - including some useful lua examples for beginners
- [luatool -  Small python script for loading init.lua to ESP8266 nodemcu firmware](https://github.com/4refr0nt/luatool)
- [eLua Docs & Specification](http://www.eluaproject.net/doc/v0.9/en_refman_gen.html)
- [eLua examples](https://github.com/elua/examples)
- [ESPlorer SDK GUI](http://esp8266.ru/esplorer/)
- [ESPSuperdumper](http://www.electrodragon.com/w/ESP8266)
- [I2S MP3 webradio streaming](https://github.com/espressif/esp8266_mp3_decoder)
